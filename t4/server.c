#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>

#define PORT 5000
#define MAX_CLIENTES 50
#define MAX_SIZE_BUFFER 1000

int server_socket_fd;
int sockets_clientes[MAX_CLIENTES];
fd_set read_fd_set;

int adiciona_cliente(int cliente_fd);
void trata_servidor();
void trata_conexao(int socket_cliente, int index);

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
      printf("Digite: %s <ip> <porta>\n", argv[0]);
      exit(1);
    }

    // Cria Socket
    server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_fd < 0)
    {
        exit(1);
    }

    // Configura endereço do servidor
    struct sockaddr_in end_servidor;
    memset(&end_servidor, 0, sizeof(end_servidor));
    end_servidor.sin_family = AF_INET;
    end_servidor.sin_addr.s_addr = inet_addr(argv[1]);
    end_servidor.sin_port = htons(atoi(argv[2]));

    // Etapa de BIND
    if (bind(server_socket_fd, (struct sockaddr *)&end_servidor, sizeof(end_servidor)) < 0)
    {
        exit(1);
    }

    FD_ZERO(&read_fd_set);

    // Setando os sockets dos clientes como inválidos
    for (int i = 0; i < MAX_CLIENTES; i++)
    {
        sockets_clientes[i] = -1;
    }

    // Escutando conexões
    if (listen(server_socket_fd, 10) < 0)
    {
        exit(1);
    }

    // Loop principal de conversa entre clientes e servidor
    while (1)
    {
        FD_ZERO(&read_fd_set);
        FD_SET(server_socket_fd, &read_fd_set);

        for (int i = 0; i < MAX_CLIENTES; i++)
        {
            if (sockets_clientes[i] > 0)
            {
                FD_SET(sockets_clientes[i], &read_fd_set);
            }
        }

        // Preparando o select
        if (select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0)
        {
            exit(1);
        }

        // Conferindo se o socket já foi cadastrado
        if (FD_ISSET(server_socket_fd, &read_fd_set))
        {
            trata_servidor();
        }

        // Trantando a conexão de todos os servidores válidos
        for (int i = 0; i < 30; i++)
        {
            int new_client_socket_fd = sockets_clientes[i];

            if (new_client_socket_fd == -1)
                continue;

            if (FD_ISSET(new_client_socket_fd, &read_fd_set))
            {
                trata_conexao(new_client_socket_fd, i);
            }
        }
    }

    return 0;
}

int adiciona_cliente(int cliente_fd)
{

    // Adiciona novo cliente
    int i;
    for (i = 0; i < MAX_CLIENTES; i++)
    {
        if (sockets_clientes[i] == -1)
        {
            sockets_clientes[i] = cliente_fd;
            break;
        }
    }

    // Caso o número maximo de clientes seja alcançado obtemos um erro
    if (i == MAX_CLIENTES)
    {
        return -1;
    }

    return i;
}

void trata_servidor()
{
    // Configurando endereço de novas conexões
    struct sockaddr_in end_cliente;
    unsigned int end_cliente_tamanho = sizeof(end_cliente);
    memset(&end_cliente, 0, sizeof(end_cliente));

    int client_socket_fd = accept(
        server_socket_fd,
        (struct sockaddr *)&end_cliente,
        &end_cliente_tamanho);

    // Adicionando novo cliente e checando se deu certo
    if (adiciona_cliente(client_socket_fd) == -1)
    {
        printf("\nNão foi possivel adicionar novo cliente\n");
        exit(-1);
    }

    printf("\nNovo cliente adicionado %d\n", client_socket_fd);
}

void trata_conexao(int socket_cliente, int index)
{

    char buffer[MAX_SIZE_BUFFER * 2];

    // Espera e calcula o tamanho da mensagem recebida
    int bytes_recebidos = recv(socket_cliente, buffer, MAX_SIZE_BUFFER, 0);

    // Caso não receba nada, fecha a conexão e fecha o socket do cliente
    if (bytes_recebidos < 1)
    {
        printf("\nCliente %d fechando conexão\n", socket_cliente);

        close(socket_cliente);
        sockets_clientes[index] = -1;
    }

    // Delimitando a mensagem recebida
    buffer[bytes_recebidos] = '\0';
    printf("\n-> Cliente %d: %s", socket_cliente, buffer);

    // Enviando a mensagem pros outros clientes
    for (int i = 0; i < MAX_CLIENTES; i++)
    {
        int proximo_cliente = sockets_clientes[i];

        if (proximo_cliente == -1 || proximo_cliente == socket_cliente)
            continue;

        send(proximo_cliente, buffer, bytes_recebidos, 0);
    }
}
