#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>

const int tamanho_maximo_mensagem = 1000;

int pid_filho = 0;

int aguarda_mensagens(int socket);
void envia_mensagem(int socket);

int main(int argc, char *argv[])
{
    int socket_fd;

	if (argc < 3)
	{
		printf("Digite: %s <ip_servidor> <porta_servidor>\n", argv[0]);
		exit(1);
	}

    // Abertura de código
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {

        printf("Falha na criação do socket\n");
        exit(-1);
    }

    // Configuração do endereço do socket
    struct sockaddr_in end_servidor;
    memset(&end_servidor, 0, sizeof(end_servidor));

	end_servidor.sin_family = AF_INET; 
	end_servidor.sin_addr.s_addr = inet_addr(argv[1]);
	end_servidor.sin_port = htons(atoi(argv[2]));

    // Conecta ao servidor
    if (connect(
            socket_fd,
            (struct sockaddr *)&end_servidor,
            sizeof(end_servidor)) != 0)
    {
        printf("Erro na conexão \n");
        exit(-1);
    }

    // Divisão entre dois processor para ler mensagens para enviar, e para ler mensagens do servidor
    pid_filho = fork();

    if (pid_filho == 0)
    {
        envia_mensagem(socket_fd);
    }
    else
    {
        while (1)
        {
            if (aguarda_mensagens(socket_fd) < 1)
            {
                break;
            }
        }

        kill(pid_filho, SIGINT);
    }

    close(socket_fd);

    return 0;
}

int aguarda_mensagens(int socket)
{
    char buffer[tamanho_maximo_mensagem];

    // Esperando receber mensagem
    int total_bytes_recebidos = recv(socket, buffer, tamanho_maximo_mensagem, 0);

    // Conferindo o tamanho da mensagem recebida
    if (total_bytes_recebidos < 0)
    {
        printf("Erro na conexão\n");
    }
    else if (total_bytes_recebidos > 0)
    {
        buffer[total_bytes_recebidos] = '\0';
        printf("\n-> Servidor: %s\n", buffer);
    }

    return total_bytes_recebidos;
}

void envia_mensagem(int socket)
{

    char buffer[tamanho_maximo_mensagem];

    // Loop para esperar o usuário digitar uma mensagem e enviar
    while (1)
    {
        printf("-> Digite sua mensagem:  ");

        char c;
        int size = 0;
        do
        {
            c = getchar();
            buffer[size++] = c;
        } while (c != '\n');

                buffer[size++] = '\0';

        if (write(socket, buffer, size) < 1)
        {
            break;
        }
    }
}
