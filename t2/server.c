#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define QTD 5
#define MAX 80

int trata_cliente(int soc, struct sockaddr_in end_cliente);

int main(int argc, char *argv[]) {
   struct sockaddr_in end_servidor;
   struct sockaddr_in end_cliente;
   int soc_servidor, novo_soc;
   int tam_client;

   if (argc<3) {
	  printf("Digite: %s <ip> <porta>\n", argv[0]);
	  exit(1);
   }

   // Define as configurações de endereço do servidor
   end_servidor.sin_family = AF_INET;
   end_servidor.sin_addr.s_addr = inet_addr(argv[1]);
   end_servidor.sin_port = htons(atoi(argv[2]));

   // Cria socket
   soc_servidor = socket(AF_INET, SOCK_STREAM, 0);
   if (soc_servidor < 0) {
     printf("Falha no socket\n");
     exit(1);
   }

   // Faz a bind entre o socket e endereço
   if (bind(soc_servidor, (struct sockaddr *)&end_servidor, sizeof(end_servidor)) < 0) {
     printf("Falha no bind\n");
     exit(1);
   } 

   // Escuta a porta do socket
   if (listen(soc_servidor, QTD) < 0) {
     printf("Falha no listen\n");
     exit(1);
   }

   printf("Atendendo no IP %s porta %s ...\n", argv[1], argv[2]);
   tam_client = sizeof(end_cliente);

   // Trata envio e recebimento de dados
   while(1){
     // Cria novo socket do cliente
     if ((novo_soc=accept(soc_servidor, (struct sockaddr *)&end_cliente, &tam_client)) < 0) {
         fprintf(stdout, "Falha na conexao\n");
         exit(1);
     }
     printf("{%s: %u conectado}\n", inet_ntoa(end_cliente.sin_addr), ntohs(end_cliente.sin_port));
     trata_cliente(novo_soc, end_cliente);
   }
}

int trata_cliente(int soc, struct sockaddr_in end_cliente)  {
   char msg[MAX];

    while (1) {
        memset(&msg, 0x0, sizeof(msg)); // Limpa buffer
        // Recebe dados enviados pelo cliente
		if(recv(soc, &msg, sizeof(msg),0) < 0) {
            printf("Nao pode receber dados \n");
            continue;
        } 
        if (strncmp(msg, "FIM", 3) == 0) break;

        printf("[%s:%u] => %s\n", inet_ntoa(end_cliente.sin_addr), ntohs(end_cliente.sin_port), msg);
        printf("> ");
        scanf(" %[^\n]s", msg);
        // Envia dados para o cliente
        send(soc, &msg, strlen(msg), 0);
    }
    printf("Conexão %s:%u encerrada.\n", inet_ntoa(end_cliente.sin_addr), ntohs(end_cliente.sin_port));
    close (soc);
 }
