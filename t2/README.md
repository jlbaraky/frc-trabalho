# Trabalho FRC - 2020/2 - Exercício 2

## Integrantes

- João Luis Baraky - 18/0033646
- Ezequiel Oliveira - 16/0119316

## Execução

Para executar o programa, é necessário primeiro compilar os arquivos:

`$ gcc server.c -o server`

`$ gcc client.c -o client` 

Depois disso, basta rodar o servidor e depois o(s) cliente(s):

`$ ./server <ip> <porta>`

`$ ./client <ip_servidor> <porta_servidor>` 

## Utilização

A utilização é mediante envio de mensagens, como nos exemplos abaixo:

![t2-1](https://i.imgur.com/ckMCzpG.png)

![t2-2](https://i.imgur.com/I3QQj1b.png)
