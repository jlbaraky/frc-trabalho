# Trabalho FRC - 2020/2 - Exercício 1

## Integrantes

- João Luis Baraky - 18/0033646
- Ezequiel Oliveira - 16/0119316

## Execução

Para executar o programa, é necessário primeiro compilar os arquivos:

`$ gcc server.c -o server`

`$ gcc client.c -o client` 

Depois disso, basta rodar o servidor e depois o(s) cliente(s):

`$ ./server <ip> <porta>`

`$ ./client <ip_servidor> <porta_servidor>` 

## Utilização

A utilização é mediante envio de mensagens, como nos exemplos abaixo:

![t1-1](https://i.imgur.com/eCH7Ab1.png)

![t1-2](https://i.imgur.com/e3y36j3.png)


