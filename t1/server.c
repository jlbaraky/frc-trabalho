#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h> 
#include <string.h> 
#include <stdlib.h>

#define MAX 100

int main(int argc, char *argv[]) {
  int soc_servidor, n, tamanho_cliente;
  struct sockaddr_in end_cliente;
  struct sockaddr_in end_servidor;
  char msg[MAX];

  if (argc<3) {
	  printf("Digite: %s <ip> <porta>\n", argv[0]);
	  exit(1);
  } 

  // Cria socket
  soc_servidor = socket(AF_INET, SOCK_DGRAM, 0);
  if(soc_servidor<0) {
      printf("Falha no socket\n");
      exit(1);
  } 


  // Define configurações de endereço
  end_servidor.sin_family = AF_INET;
  end_servidor.sin_addr.s_addr = inet_addr(argv[1]); 
  end_servidor.sin_port = htons(atoi(argv[2]));

  // Faz o bind com o socket do servidor e seu endereço
  if(bind (soc_servidor, (struct sockaddr *) &end_servidor,sizeof(end_servidor)) < 0) {
      printf("Falha no bind\n");
      exit(1);
  }

  printf("Atendendo no IP: %s, porta UDP: %s\n", argv[1], argv[2]);


  // Trata recebimento e envio de dados
  while(1) {
      memset(msg,0x0,MAX); // Limpa buffer
      tamanho_cliente = sizeof(end_cliente);

      // Recebe dados enviados pelo cliente
      if(recvfrom(soc_servidor, msg, MAX, 0, (struct sockaddr *) &end_cliente, &tamanho_cliente) < 0) {
          printf("Falha no recebimento dos dados\n");
          continue;
      } 
      printf("[IP: %s, Porta: %u] > %s\n",inet_ntoa(end_cliente.sin_addr), ntohs(end_cliente.sin_port), msg);
      printf("> ");
      scanf(" %[^\n]s", msg);
      
      // Envia dados ao cliente
      if(sendto(soc_servidor, msg, strlen(msg), 0,(struct sockaddr *) &end_cliente, sizeof(end_cliente)) < 0) {
          printf("Falha no envio dos dados\n");
          close(soc_servidor);
          exit(1);
      }
  }

  return 0;
}
