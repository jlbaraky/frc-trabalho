#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define MAX 100

int main(int argc, char *argv[]) {
  int soc_client, tamanho_servidor;
  char msg[MAX];
  struct sockaddr_in end_cliente;
  struct sockaddr_in end_servidor;
  
  if(argc<3) {
    printf("Digite: %s <ip_servidor> <portao_servidor> \n", argv[0]);
    exit(1);
  }

  // Define configurações de endereço do servidor
  end_servidor.sin_family 	   = AF_INET;
  end_servidor.sin_addr.s_addr = inet_addr(argv[1]);
  end_servidor.sin_port 	   = htons(atoi(argv[2]));


  // Define configurações de endereço do cliente
  end_cliente.sin_family 	 = AF_INET;
  end_cliente.sin_addr.s_addr= htonl(INADDR_ANY);  
  end_cliente.sin_port 	     = htons(0);

  // Cria socket
  soc_client = socket(AF_INET,SOCK_DGRAM,0);
  if(soc_client < 0) {
    printf("Falha no socket\n");
    exit(1);
  }
   
  // Faz o bind entre o socket e o endereço
  if(bind(soc_client, (struct sockaddr *) &end_cliente, sizeof(end_cliente)) < 0) {
    printf("Falha no bind\n");
    exit(1);
  }

  printf("{Servidor - IP: %s, Porta: %s}\n", argv[1], argv[2]);


  // Trata envio e recebimento de dados
  while(1) {
      printf("> ");
      scanf(" %[^\n]s", msg);

      // Envia dados para o servidor
      if(sendto(soc_client, msg, strlen(msg), 0,(struct sockaddr *) &end_servidor, sizeof(end_servidor)) < 0) {
          printf("Falha no envio de dados\n");
          close(soc_client);
          exit(1);
      }
   	  memset(msg,0x0,MAX); // Limpa buffer
      tamanho_servidor = sizeof(end_servidor);

      // Recebe dados enviados pelo servidor
      if(recvfrom(soc_client, msg, MAX, 0, (struct sockaddr *) &end_servidor, &tamanho_servidor) < 0) {
          printf("Falha no recebimento de dados \n");
          continue;
      } 

      printf("[IP: %s, Porta: %u] > %s\n",inet_ntoa(end_servidor.sin_addr), ntohs(end_servidor.sin_port), msg);
  }

  return 0;
}
