#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 100

int main(int argc, char *argv[])
{
	struct sockaddr_in end_servidor;
	int soc_cliente;
	char msg[MAX];

	if (argc < 3)
	{
		printf("Digite: %s <ip_servidor> <porta_servidor>\n", argv[0]);
		exit(1);
	}

    // Define configurações de endereço do servidor
	end_servidor.sin_family = AF_INET; 
	end_servidor.sin_addr.s_addr = inet_addr(argv[1]);
	end_servidor.sin_port = htons(atoi(argv[2]));

	// Cria socket do cliente
	soc_cliente = socket(AF_INET, SOCK_STREAM, 0);
	if (soc_cliente < 0)
	{
		printf("Falha no socket\n");
		exit(1);
	}

	// Conecta com o servidor
	if (connect(soc_cliente, (struct sockaddr *)&end_servidor, sizeof(end_servidor)) < 0)
	{
		printf("Falha ao conectar\n");
		exit(1);
	}

    // Trata envio e recebimento de dados
	while (1)
	{
		printf("> ");
		scanf(" %[^\n]s", msg);
		// Envia dados para o servidor
		send(soc_cliente, &msg, strlen(msg), 0);
		if (strncmp(msg, "FIM", 3) == 0) break;
		memset(&msg, 0x0, sizeof(msg)); // Limpa buffer
		// Recebe dados enviados pelo servidos
		if (recv(soc_cliente, &msg, sizeof(msg), 0) < 0)
		{
			printf("Falha no recimento de dados\n");
			continue;
		}
		printf("[%s:%u] => %s\n", inet_ntoa(end_servidor.sin_addr), ntohs(end_servidor.sin_port), msg);
	}
	close(soc_cliente);
	return (0);
}
