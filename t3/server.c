#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 100

int trata_cliente(int descritor, struct sockaddr_in end_cliente);

int main(int argc, char *argv[])
{
  struct sockaddr_in end_servidor;
  struct sockaddr_in end_cliente;
  int soc_servidor, novo_soc;
  int tam_cliente;

  if (argc < 3)
  {
    printf("Digite: %s <ip> <porta>\n", argv[0]);
    exit(1);
  }

  // Define onfiguração do endereço do servidor
  end_servidor.sin_family = AF_INET;
  end_servidor.sin_addr.s_addr = inet_addr(argv[1]);
  end_servidor.sin_port = htons(atoi(argv[2]));

  // Cria socket
  soc_servidor = socket(AF_INET, SOCK_STREAM, 0);
  if (soc_servidor < 0)
  {
    fprintf(stderr, "Falha ao criar socket!\n");
    exit(1);
  }

  // Faz o bind com as portas
  if (bind(soc_servidor, (struct sockaddr *)&end_servidor, sizeof(end_servidor)) < 0)
  {
    printf("Falha no bind\n");
    exit(1);
  }

  // Ouvindo a porta
  if (listen(soc_servidor, 10) < 0)
  {
    printf( "Falha no listen\n");
    exit(1);
  }

  printf("Atendendo no IP %s porta %s ...\n\n", argv[1], argv[2]);
  tam_cliente = sizeof(end_cliente);

  // Trata envio e recebimento de dados
  while (1)
  {
    // Cria novo socket do cliente
    if ((novo_soc = accept(soc_servidor, (struct sockaddr *)&end_cliente, &tam_cliente)) < 0)
    {
      printf("Falha na conexao\n");
      exit(1);
    }
    else
    {
      if (fork() == 0)
      {
        printf("{Cliente %s: %u conectado.}\n", inet_ntoa(end_cliente.sin_addr), ntohs(end_cliente.sin_port));
        trata_cliente(novo_soc, end_cliente);
        return 0;
      }
    }
  }
}

int trata_cliente(int soc, struct sockaddr_in end_cliente)
{
  char msg[MAX];

  while (1)
  {
    memset(&msg, 0x0, sizeof(msg)); // Limpa buffer
    // Recebe dados enviados pelo cliente
    if (recv(soc, &msg, sizeof(msg), 0) < 0)
    {
      printf("Falha no recebimento de dados\n");
      continue;
    }
    if (strncmp(msg, "FIM", 3) == 0) break;

    printf("[%s:%u] => %s\n", inet_ntoa(end_cliente.sin_addr), ntohs(end_cliente.sin_port), msg);
    scanf(" %[^\n]s", msg);
    // Envia dados pra o cliente
    send(soc, &msg, strlen(msg), 0);
  }
  close(soc);
}
